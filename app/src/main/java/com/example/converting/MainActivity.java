package com.example.converting;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import static java.lang.Double.parseDouble;

public class MainActivity extends AppCompatActivity {
    private EditText input;
    private EditText output;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        input = (EditText) findViewById(R.id.mm);
        output = (EditText) findViewById(R.id.inchess);


    }
    public void Convert(View v){
        double mm= parseDouble(input.getText().toString());
        double inches=mm/25.4;

        output.setText(Double.toString(inches));
    }


    public void Exit(View v) {

        finish();
        System.exit(0);
    }
}